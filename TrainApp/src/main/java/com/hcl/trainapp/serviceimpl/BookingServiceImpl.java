package com.hcl.trainapp.serviceimpl;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.trainapp.exception.UserDefinedException;
import com.hcl.trainapp.model.Booking;
import com.hcl.trainapp.model.Passengers;
import com.hcl.trainapp.model.Train;
import com.hcl.trainapp.model.User;
import com.hcl.trainapp.repository.BookingRepository;
import com.hcl.trainapp.repository.TrainRepository;
import com.hcl.trainapp.repository.UserRepository;
import com.hcl.trainapp.service.BookingService;
@Service
public class BookingServiceImpl implements BookingService {
@Autowired
UserRepository userRepository;
@Autowired
TrainRepository trainRepository;
@Autowired
BookingRepository bookingRepository;

	@Override
	public String bookTicket(Long userId, String date, int trainNumber, List<@Valid Passengers> passengerList) throws UserDefinedException, ParseException{
		User user1=new User();
		user1=userRepository.findByUserId(userId);
		if(user1 == null) {
			throw new UserDefinedException("user does not exists");
		}
		Train train=new Train();
		train=trainRepository.findByTrainNumber(trainNumber);
		if(train == null) {
			throw new UserDefinedException("invalid train number");
		}
		if(passengerList.isEmpty()) {
			throw new UserDefinedException("add passengers to continue booking");
		}
		DateFormat date2=new SimpleDateFormat("dd-MM-yyyy");
		Date journeyDate=date2.parse(date);
		if(journeyDate.compareTo(new Date()) < 0) 
				throw new UserDefinedException("please provide future or current date"); 
		double totalFare=0;
		Booking booking=new Booking();
		booking.setTrain(train);
		booking.setUser(user1);
		booking.setPassengerList(passengerList);
		booking.setDate(journeyDate);
		
		for(int i=0;i<passengerList.size();i++) {
			if(passengerList.get(i).getAge() > 4) {
				totalFare += train.getCost_single_seats();
			}		
		}
		booking.setPrice_total(totalFare);
		bookingRepository.saveAndFlush(booking);
		return "booking done successfully";
	}

	@Override
	public List<Booking> getTickets(Long userId) throws UserDefinedException {
		User user=new User();
		user=userRepository.findByUserId(userId);
		if(user==null) {
			throw new UserDefinedException("user does not exist");
			
		}
		List <Booking> bookings=new ArrayList<Booking>();
		bookings=bookingRepository.getBookings(userId);
		if(bookings.isEmpty()) {
			throw new UserDefinedException("no booking history found");
		}
		return bookings;
		
	}


}
