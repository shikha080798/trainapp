package com.hcl.trainapp.dto;

import java.util.List;

import com.hcl.trainapp.model.Passengers;

public class BookingResponseDto {
	private Long userId;
	private String date;
	private int trainNumber;
	private List<Passengers> passengerList;
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getTrainNumber() {
		return trainNumber;
	}
	public void setTrainNumber(int trainNumber) {
		this.trainNumber = trainNumber;
	}
	public List<Passengers> getPassengerList() {
		return passengerList;
	}
	public void setPassengerList(List<Passengers> passengerList) {
		this.passengerList = passengerList;
	}
	

}
