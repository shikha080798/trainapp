package com.hcl.trainapp.controller;

import javax.validation.Valid;
import javax.validation.constraints.Min;

 

//import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

 


import com.hcl.trainapp.dto.UserDto;
import com.hcl.trainapp.serviceimpl.UserServiceImpl;

 

@RestController
public class UserController {

 

    @Autowired
    UserServiceImpl userService;
    @PostMapping("/users/otp")
    public ResponseEntity<String> otpvalidate(@Valid @Min(1) @RequestBody Long userId,@Valid @RequestBody Long otp) {
        return userService.otp(userId, otp);
    }
    @PostMapping("/users")
    public ResponseEntity<?> sendotp(@RequestBody  @Valid UserDto userDto){
        return userService.userLogin(userDto.getUserName());
    
    }

 

    
    }
 