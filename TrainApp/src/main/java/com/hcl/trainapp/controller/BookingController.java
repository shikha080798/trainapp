package com.hcl.trainapp.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.trainapp.dto.BookingResponseDto;
import com.hcl.trainapp.exception.TrainNotFoundException;
import com.hcl.trainapp.exception.UserDefinedException;
import com.hcl.trainapp.model.Booking;
import com.hcl.trainapp.model.Passengers;
import com.hcl.trainapp.model.Train;
import com.hcl.trainapp.model.User;
import com.hcl.trainapp.service.BookingService;

@RestController
@Validated
public class BookingController {

	@Autowired
	BookingService bookingService;

	@PostMapping("/app/tickets")
	public ResponseEntity<String> bookTickets(@RequestParam Long userId, @RequestParam String date,
			@RequestParam int trainNumber, @RequestBody @Valid List<Passengers> passengerList)
			throws ParseException, UserDefinedException {
		return new ResponseEntity<String>(bookingService.bookTicket(userId, date, trainNumber, passengerList),
				HttpStatus.OK);
	}
//	@PostMapping("/app/tickets")
//	public ResponseEntity<String> bookTickets(BookingResponseDto bookingResponseDto)
//			throws ParseException, UserDefinedException {
//		return new ResponseEntity<String>(bookingService.bookTicket(bookingResponseDto),HttpStatus.OK);
//	}
	
	
	
	
	
	
	/*
	 * @PostMapping("/app/tickets") public ResponseEntity<String>
	 * bookTickets(@RequestBody User user ,@RequestBody Train train , @RequestParam
	 * String date,
	 * 
	 * @RequestBody ArrayList< @Valid Passengers> passengerList) throws
	 * ParseException, UserDefinedException { return new
	 * ResponseEntity<String>(bookingService.bookTicket(user,train,
	 * passengerList,date), HttpStatus.OK); }
	 */

	@GetMapping("app/tickets/history")
	public ResponseEntity<List<Booking>> getTickets(@RequestParam Long userId) throws UserDefinedException {
		return new ResponseEntity<List<Booking>>(bookingService.getTickets(userId), HttpStatus.OK);
	}

}
