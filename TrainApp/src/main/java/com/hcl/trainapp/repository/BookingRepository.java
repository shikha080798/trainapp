package com.hcl.trainapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.hcl.trainapp.model.Booking;

public interface BookingRepository extends JpaRepository<Booking , Integer> {
	@Query("from Booking , User u where u.userId=:userId")
	List<Booking> getBookings(Long userId);

}
