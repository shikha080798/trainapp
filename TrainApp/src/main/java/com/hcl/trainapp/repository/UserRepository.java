package com.hcl.trainapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.trainapp.dto.BookingResponseDto;
import com.hcl.trainapp.model.User;
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
	//User findByUserIdAndPassword(int userId, String password);

	User findByUserId(Long userId);
	

	    
	    // public User findByUserId(int userId);
	     //public User findByUserIdAndPassword(int userId,String password);

	 

	    public User findByUserName(String userName);



		//User findByUserDetails(BookingResponseDto bookingResponseDto);

	 


	}


