package com.hcl.trainapp.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import com.hcl.trainapp.dto.BookingResponseDto;
import com.hcl.trainapp.exception.UserDefinedException;
import com.hcl.trainapp.model.Booking;
import com.hcl.trainapp.model.Passengers;
import com.hcl.trainapp.model.Train;
import com.hcl.trainapp.model.User;

public interface BookingService {

	//String bookTicket(Long userId, String date, int trainNumber, List<@Valid Passengers> passengerList) throws ParseException, UserDefinedException;

	//List<Booking> getTickets(Long userId) throws UserDefinedException;

	List<Booking> getTickets(Long userId) throws UserDefinedException;

	//String bookTicket(BookingResponseDto bookingResponseDto);

	String bookTicket(Long userId, String date, int trainNumber, @Valid List<Passengers> passengerList) throws UserDefinedException, ParseException;

	//-------//String bookTicket(User user, Train train, ArrayList<@Valid Passengers> passengerList);

	//String bookTicket(User user, String date,Train train, ArrayList<@Valid Passengers> passengerList);

}
