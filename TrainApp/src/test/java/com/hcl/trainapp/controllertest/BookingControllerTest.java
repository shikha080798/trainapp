package com.hcl.trainapp.controllertest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import com.hcl.trainapp.controller.BookingController;
import com.hcl.trainapp.exception.UserDefinedException;
import com.hcl.trainapp.model.Booking;
import com.hcl.trainapp.model.Passengers;
import com.hcl.trainapp.model.Train;
import com.hcl.trainapp.model.User;
import com.hcl.trainapp.serviceimpl.BookingServiceImpl;

@ExtendWith(MockitoExtension.class)

public class BookingControllerTest {
	@Mock
	BookingServiceImpl bookingServiceImpl;
	@InjectMocks
	BookingController bookingController;

	static User user;
	static Train train;
	static Passengers passenger;
	static Booking ticket;

	@BeforeAll
	public static void setUp() {
		user = new User();
		user.setUserId(1L);
		user.setUserName("virat");
		user.setPhoneNumber("9908176684");
		user.setEmail("abc98@gmail.com");
		user.setOtp(7893749L);
		train = new Train();
		train.setTrainId(1);
		train.setCost_single_seats(400L);
		train.setTrainName("Express");
		train.setFromsource("vishakapatnam");
		train.setTodestination("raipur");
		train.setSeats(100);
	}

	@Test
	@DisplayName(value = "Booking controllerTest")
	public void testBookingController() throws ParseException, UserDefinedException {
		ArrayList<Passengers> list = new ArrayList<Passengers>();
		list.add(passenger);
		when(bookingServiceImpl.bookTicket(1L, "21-09-2021",123456, list)).thenReturn("Booking done successfully");
		ResponseEntity<String> resultEntity = bookingController.bookTickets(1L, "21-09-2021",123456, list);
		assertEquals("Booking done successfully", resultEntity.getBody());
		assertThat(resultEntity.getStatusCodeValue()).isEqualTo(200);

	}

	@Test
	public void getTickets() throws UserDefinedException {
		List<Booking> booking = new ArrayList<>();
		ticket = new Booking();
		ticket.setPrice_total(500d);
		ticket.setBookingId(1);
		ticket.setTrain(train);
		ticket.setUser(user);
		booking.add(ticket);
		when(bookingServiceImpl.getTickets(1L)).thenReturn(booking);
		ResponseEntity<List<Booking>> result = bookingController.getTickets(1L);
		verify(bookingServiceImpl).getTickets(1L);
		assertEquals(booking, result.getBody());
	}

}