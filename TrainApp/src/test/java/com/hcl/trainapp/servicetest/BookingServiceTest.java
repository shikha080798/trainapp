package com.hcl.trainapp.servicetest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import com.hcl.trainapp.exception.UserDefinedException;
import com.hcl.trainapp.model.Booking;
import com.hcl.trainapp.model.Passengers;
import com.hcl.trainapp.model.Train;
import com.hcl.trainapp.model.User;
import com.hcl.trainapp.repository.TrainRepository;
import com.hcl.trainapp.repository.UserRepository;
import com.hcl.trainapp.service.BookingService;
import com.hcl.trainapp.service.TrainService;
import com.hcl.trainapp.serviceimpl.TrainServiceImpl;
@ExtendWith(MockitoExtension.class)
public class BookingServiceTest {
	@Mock
	TrainRepository trainRepository;
	@InjectMocks
	TrainServiceImpl trainService;
	@Autowired
	UserRepository userRepository;
	@Autowired
	BookingService bookingService;
	static User user;
	static Train train;
	static Passengers passenger;
	static Booking booking;
	
	@BeforeAll
	public static void setUp() {
		user=new User();
		user.setUserId(1L);
		user.setUserName("sai");
		user.setPhoneNumber("8076442200");
		user.setOtp(809890L);
		user.setEmail("sai@gmail.com");
		
		train =new Train();
		train.setTrainId(1);
		train.setCost_single_seats(400L);
		train.setTrainName("special");
		train.setFromsource("delhi");
		train.setTodestination("raipur");
		train.setSeats(200);
		train.setTrainNumber(8080);
		
		passenger =new Passengers();
		passenger.setAadharNo(102020202020L);
		passenger.setAddress("Pune");
		passenger.setAge(20);
		passenger.setName("sai");
	}
	@Test
	@DisplayName("Booking Tickets : Positive Scenario")
	public void testBooking1()throws ParseException, UserDefinedException{
		when(userRepository.findByUserId(1L)).thenReturn(user);
		when(trainRepository.findByTrainNumber(8080)).thenReturn(train);
		List<Passengers>list=new ArrayList<Passengers>();
		list.add(passenger);
		assertTrue(list.size()!=0);
		DateFormat date2=new SimpleDateFormat("dd-MM-yyyy");
		Date journeyDate=(Date)date2.parse("21-09-2021");
		Booking booking=new Booking();
		booking.setTrain(train);
		booking.setUser(user);
		booking.setPassengerList(list);
		booking.setDate(journeyDate);
		String resultString=bookingService.bookTicket(1L,"21-09-2021",8080, list);
		assertEquals("booking done successfully",resultString);
	
	}
	
	@Test
	@DisplayName("Booking Tickets : Negative Scenario")
	public void testBooking2()throws ParseException, UserDefinedException{
		when(userRepository.findByUserId(1L)).thenReturn(null);
		List<Passengers>list=new ArrayList<Passengers>();
		list.add(passenger);
		assertThrows(UserDefinedException.class, () -> bookingService.bookTicket(1L,"21-09-2021",1233,list));		
	}
	@Test
	@DisplayName("Booking Tickets : Negative Scenario")
	public void testBooking3()throws ParseException, UserDefinedException{
		when(trainRepository.findByTrainNumber(8080)).thenReturn(null);
		when(userRepository.findByUserId(1L)).thenReturn(user);
		List<Passengers>list=new ArrayList<Passengers>();
		list.add(passenger);
		assertThrows(UserDefinedException.class, () -> bookingService.bookTicket(1L,"21-09-2021",8080,list));		
	}
	@Test
	@DisplayName("Booking Tickets : Negative Scenario")
	public void testBooking4()throws ParseException, UserDefinedException{
		when(trainRepository.findByTrainNumber(8080)).thenReturn(train);
		when(userRepository.findByUserId(1L)).thenReturn(user);
		List<Passengers>list=new ArrayList<Passengers>();
		list.add(passenger);
		assertThrows(UserDefinedException.class, () -> bookingService.bookTicket(1L,"21-09-2021",8080,list));		
	}
	
}
